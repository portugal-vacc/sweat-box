import re
from typing import Optional

from src.models import (
    Scenario,
    RunwayILS,
    Hold,
    AirportAltitude,
    Metar,
    Controller,
    Aircraft,
    AircraftPosition,
    FlightPlan,
    Route,
    InitialPseudoPilot,
    SimData,
)


def parse_ils(raw_text: str) -> list[Optional[RunwayILS]]:
    """Regex needs to be fixed..."""
    matches = re.finditer(
        r"ILS(?P<runway_name>\w*):(?P<threshold_latitude>\d+.\d+):(?P<threshold_longitude>(?:-|\+)\d+.\d+):(?:(?P<runway_heading>\d{3})|(?P<far_end_latitude>.*):(?P<far_end_longitude>.*))",
        raw_text,
    )
    return [RunwayILS(**match.groupdict()) for match in matches]


def parse_holdings(raw_text: str) -> list[Optional[Hold]]:
    matches = re.finditer(
        r"HOLDING:(?P<fix_name>\w*):(?P<inbound_course>.*):(?P<direction>.*)", raw_text
    )
    return [Hold(**match.groupdict()) for match in matches]


def parse_airport_altitude(raw_text: str) -> Optional[AirportAltitude]:
    matches = re.finditer(r"AIRPORT_ALT:(?P<altitude>.*)", raw_text)
    for match in matches:
        return AirportAltitude(**match.groupdict())
    return None


def parse_controllers(raw_text: str) -> list[Optional[Controller]]:
    matches = re.finditer(r"CONTROLLER:(?P<call_sign>.*):(?P<frequency>.*)", raw_text)
    return [Controller(**match.groupdict()) for match in matches]


def parse_metars(raw_text: str) -> list[Optional[Metar]]:
    matches = re.finditer(r"METAR:(?P<metar>.*)", raw_text)
    return [Metar(**match.groupdict()) for match in matches]


def parse_aircraft_position(raw_text: str) -> AircraftPosition | None:
    aircraft_position_re = re.search(
        r"@(?P<transponder_flag>\w):(?P<call_sign>\w+):(?P<squawk_code>\d{4}):1:(?P<latitude>.+):(?P<longitude>.+):(?P<altitude>\d+):0:(?P<heading>\d+):0",
        raw_text,
    )
    if not aircraft_position_re:
        return None
    to_return = aircraft_position_re.groupdict()
    to_return["heading"] = float(
        ((int(to_return["heading"]) / 2.88) - 0.5) / 4
    )  # ( Heading * 2.88 + 0.5 ) * 4
    return AircraftPosition(**to_return)


def parse_aircraft_flight_plan(raw_text: str) -> FlightPlan | None:
    flight_plan_re = re.search(
        r"\$FP(?P<call_sign>.*):\*A:(?P<flight_plan_type>\w)?:(?P<aircraft_type>.*)?:(?P<true_air_speed>\d+)?:(?P<origin_airport>\w+)?:(?P<departure_time_est>\d+)?:(?P<departure_time_act>\d+)?:(?P<final_cruising_altitude>\d+)?:(?P<destination_airport>\w+):(?P<hrs_en_route>\d+):(?P<mins_en_route>\d+)?:(?P<hrs_fuel>\d+)?:(?P<mins_fuel>\d+)?:(?P<alternate_airport>(?:\w+|))?:(?P<remarks>.*)?:(?P<route>.*)?",
        raw_text,
    )
    return FlightPlan(**flight_plan_re.groupdict()) if flight_plan_re else None


def parse_aircraft_route(raw_text: str) -> Route | None:
    start_re = re.search(r"START:(?P<start>\d+|)", raw_text)
    start = {}
    if start_re:
        start = start_re.groupdict()

    reqalt_re = re.search(
        r"REQALT:(?P<descend_to_waypoint>\S+|):(?P<descend_to_altitude>\d+|)", raw_text
    )
    reqalt = {}
    if reqalt_re:
        reqalt = reqalt_re.groupdict()

    route_re = re.search(r"\$ROUTE:(?P<route_points>.+)", raw_text)
    if not route_re:
        return None
    to_return = route_re.groupdict()
    to_return["route_points"] = to_return["route_points"].strip()
    return Route(**to_return, **start, **reqalt)


def parse_inital_pseudo_pilot(raw_text: str) -> InitialPseudoPilot | None:
    initial_pseudo_pilot_re = re.search(
        r"INITIALPSEUDOPILOT:(?P<call_sign>\w+)", raw_text
    )
    return (
        InitialPseudoPilot(**initial_pseudo_pilot_re.groupdict())
        if initial_pseudo_pilot_re
        else None
    )


def parse_sim_data(raw_text: str) -> SimData | None:
    sim_data_re = re.search(
        r"SIMDATA:(?P<call_sign>\w+):(?P<plane_type>\S+):(?P<livery>\S+):(?P<maximum_taxi_speed>\w+):(?P<taxiway_usage>\w+):(?P<object_extent>\S+)",
        raw_text,
    )
    return SimData(**sim_data_re.groupdict()) if sim_data_re else None


def get_blocks(raw_text: str):
    if len(raw_text.split("\r\n\r\n")) != 1:
        return raw_text.split("\r\n\r\n")
    if len(raw_text.split("\n\n")) != 1:
        return raw_text.split("\n\n")
    raise NotImplementedError()


def parse_aircraft(raw_text: str) -> list[Optional[Aircraft]]:
    aircrafts = []
    for segment in get_blocks(raw_text):
        aircraft_position = parse_aircraft_position(segment)
        if not aircraft_position:
            continue
        aircraft_flight_plan = parse_aircraft_flight_plan(segment)
        aircraft_route = parse_aircraft_route(segment)
        ias_variation = None
        initial_pseudo_pilot = parse_inital_pseudo_pilot(raw_text)
        sim_data = parse_sim_data(segment)

        aircrafts.append(
            Aircraft(
                aircraft_position=aircraft_position,
                flight_plan=aircraft_flight_plan,
                route=aircraft_route,
                ias_variation=ias_variation,
                initial_pseudo_pilot=initial_pseudo_pilot,
                sim_data=sim_data,
            )
        )
    return aircrafts


def scenario_parser(raw_text: str) -> Scenario:
    return Scenario(
        runways_ils=parse_ils(raw_text),
        holdings=parse_holdings(raw_text),
        airport_altitude=parse_airport_altitude(raw_text),
        controllers=parse_controllers(raw_text),
        metars=parse_metars(raw_text),
        aircrafts=parse_aircraft(raw_text),
    )
