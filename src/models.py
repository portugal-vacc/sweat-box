from datetime import time
from typing import Optional, List

from pydantic import BaseModel, Field


class AircraftPosition(BaseModel):
    transponder_flag: str  # N for normal or S for stand by transponder mode
    call_sign: str
    squawk_code: int = 2000  # 0000 the simulator will display it as a vehicle
    latitude: (
        float  # can be the format found in the sector file or simple double values
    )
    longitude: (
        float  # can be the format found in the sector file or simple double values
    )
    altitude: int
    heading: float  # ( Heading * 2.88 + 0.5 ) * 4

    def export(self) -> str:
        return (
            f"@{self.transponder_flag}:{self.call_sign}:{self.squawk_code:04}:1:{self.latitude}:{self.longitude}:"
            f"{self.altitude}:0:{int(((self.heading * 2.88) + 0.5) * 4):03}:0"
        )


class FlightPlan(BaseModel):
    call_sign: str
    flight_plan_type: str  # V or I
    aircraft_type: str  # eg. B738 or A321
    true_air_speed: int
    origin_airport: Optional[str]
    departure_time_est: Optional[str] = "0000"  # HHmm
    departure_time_act: Optional[str] = "0000"  # HHmm
    final_cruising_altitude: int  # eg. 31000 = FL310
    destination_airport: Optional[str]
    estimated_enroute_time: Optional[time] = time(hour=0, minute=0)
    endurance: Optional[time] = time(hour=0, minute=0)
    alternate_airport: Optional[str] = ""
    remarks: Optional[str] = "/v/"
    route: str

    def export(self) -> str:
        return (
            f"$FP{self.call_sign}:*A:{self.flight_plan_type}:{self.aircraft_type}:{self.true_air_speed}:"
            f"{self.origin_airport}:{self.departure_time_est}:{self.departure_time_act}:"
            f"{self.final_cruising_altitude}:{self.destination_airport}:{self.estimated_enroute_time.hour:02}:"
            f"{self.estimated_enroute_time.minute:02}:{self.endurance.hour}:{self.endurance.minute}:"
            f"{self.alternate_airport}:{self.remarks}:{self.route.strip()}"
        )


class Route(BaseModel):
    route_points: str
    start: Optional[
        int
    ]  # start value is a wait time in minutes after that the aircraft will be added
    pilot_minimum_delay: int = Field(
        default=1, ge=1, le=30
    )  # determines how fast the pilot will respond to an order
    pilot_maximum_delay: int = Field(
        default=2, ge=2, le=31
    )  # determines how fast the pilot will respond to an order
    descend_to_waypoint: Optional[
        str
    ]  # used to set up an initial descent for the arrival planes
    descend_to_altitude: Optional[
        int
    ]  # used to set up an initial descent for the arrival planes

    def export(self) -> str:
        to_return = [f"$ROUTE:{self.route_points}"]
        if self.start:
            to_return.append(f"START:{self.start}")
        if self.pilot_minimum_delay or self.pilot_maximum_delay:
            to_return.append(
                f"DELAY:{self.pilot_minimum_delay}:{self.pilot_maximum_delay}"
            )
        if self.descend_to_waypoint or self.descend_to_altitude:
            to_return.append(
                f"REQALT:{self.descend_to_waypoint}:{self.descend_to_altitude}"
            )
        return "\n".join(to_return)


class IASVariation(BaseModel):
    """How much the plane should deviate from the normal IAS. It can be between 0 and 20."""

    deviation: int = Field(ge=0, le=20)

    def export(self) -> str:
        return f"IASVARIATION:{self.deviation}"


class InitialPseudoPilot(BaseModel):
    """This line defines who will be the pseudo pilot of the plane. If the callsign is logging in,
    the pseudo pilot state is transferred to that controller immediately."""

    call_sign: str

    def export(self) -> str:
        return f"INITIALPSEUDOPILOT:{self.call_sign}"


class SimData(BaseModel):
    """
    It determinate some additional data for tower view and ground simulations

    plane type – can be
        o * to use the one from the flight plan,
        o another type to override it. This type of aircraft should be available in the FSX or
            P3D SimObjects. Here you can also set the title field from aircraft.cfg from the
            simulator to display other vehicles such as follows Me. You also have to set the
            call sign with [VHCL] suffix.
    • livery – can be:
        o * to use the first three letters from the call sign,
        o another type to override it. This livery should be available in the FSX or P3D
            SimObjects.
    • maximum taxi speed – When you simulate other type of ground vehicles you may, would
        like to define the maximal taxi speed of them. The value can be between 5 and 50.
    • taxiway usage – It determines which taxiways can be used by the vehicle. It is very useful
        when you would like to simulate other ground vehicles than planes. See the taxiway
        definitions in the [[ESE Files Description]]. If the value there and this with bitwise AND
        is 0 the taxiway is not used by this vehicle.
    • object extent – The size of the vehicle. This value is used to detect collision on the
        ground. Set to 0 to disable this feature
    """

    call_sign: str
    plane_type: str = "*"
    livery: str = "*"
    maximum_taxi_speed: int = Field(ge=5, le=50)
    taxiway_usage: int = 3
    object_extent: float = 0.010

    def export(self) -> str:
        return (
            f"SIMDATA:{self.call_sign}:{self.plane_type}:{self.livery}:{self.maximum_taxi_speed}:"
            f"{self.taxiway_usage}:{self.object_extent}"
        )


class Aircraft(BaseModel):
    aircraft_position: AircraftPosition
    flight_plan: Optional[FlightPlan]
    route: Optional[Route]
    ias_variation: Optional[IASVariation]  # How much deviate from the normal IAS
    initial_pseudo_pilot: Optional[InitialPseudoPilot]
    sim_data: Optional[SimData]

    def export(self) -> str:
        to_export = [self.aircraft_position.export(), self.flight_plan.export()]
        to_export.append(self.sim_data.export()) if self.sim_data else None
        to_export.append(self.route.export()) if self.route else None
        to_export.append(self.ias_variation.export()) if self.ias_variation else None
        (
            to_export.append(self.initial_pseudo_pilot.export())
            if self.initial_pseudo_pilot
            else None
        )
        to_export.append("\n")
        return "\n".join(to_export)


class RunwayILS(BaseModel):
    runway_name: str
    threshold_latitude: float
    threshold_longitude: float
    runway_heading: Optional[float]
    far_end_latitude: Optional[float]
    far_end_longitude: Optional[float]

    def export(self) -> str:
        to_return = f"ILS{self.runway_name}:{self.threshold_latitude}:{self.threshold_longitude}"
        if self.runway_heading:
            to_return += f":{self.runway_heading}"
        if self.far_end_latitude and self.far_end_longitude:
            to_return += f":{self.far_end_latitude}:{self.far_end_longitude}"
        return to_return


class Hold(BaseModel):
    fix_name: str  # can be anything (VOR, NDB, FIX) from the sector file
    inbound_course: int
    direction: int  # can be 1 to indicate a right turn and -1 for a left turn.

    def export(self) -> str:
        return f"HOLDING:{self.fix_name}:{self.inbound_course}:{self.direction}"


class AirportAltitude(BaseModel):
    altitude: float

    def export(self) -> str:
        return f"AIRPORT_ALT:{self.altitude}"


class Controller(BaseModel):
    call_sign: str  # E.g LHBP_TWR
    frequency: str  # Eg. 118.1

    def export(self) -> str:
        return f"CONTROLLER:{self.call_sign}:{self.frequency}"


class Metar(BaseModel):
    metar: str

    def export(self) -> str:
        return f"METAR:{self.metar}"


class Scenario(BaseModel):
    runways_ils: List[Optional[RunwayILS]]
    holdings: List[Optional[Hold]]
    airport_altitude: Optional[AirportAltitude]
    controllers: List[Optional[Controller]]
    metars: List[Optional[Metar]]
    aircrafts: List[Optional[Aircraft]]

    def export(self) -> str:
        exports = [runway.export() for runway in self.runways_ils]
        exports.append("\n")
        exports.extend([hold.export() for hold in self.holdings])
        exports.append("\n")
        exports.append(self.airport_altitude.export())
        exports.append("\n")
        exports.extend([controller.export() for controller in self.controllers])
        exports.append("\n")
        exports.extend([metar.export() for metar in self.metars])
        exports.append("\n")
        exports.extend([aircraft.export() for aircraft in self.aircrafts])
        return "\n".join(exports)


def mock():
    return Scenario(
        runways_ils=[
            RunwayILS(
                runway_name="03",
                threshold_latitude="38.7663889",
                threshold_longitude="-9.1438889",
                runway_heading=23.5,
            )
        ],
        holdings=[Hold(fix_name="PINIM", inbound_course=114, direction=1)],
        airport_altitude=AirportAltitude(altitude=374),
        controllers=[Controller(call_sign="LPPT_APP", frequency="119.100")],
        metars=[Metar(metar="LHBP 292000Z 21004KT 1500 +SN BKN015 02/02 Q1016 NOSIG")],
        aircraft_positions=[
            Aircraft(
                aircraft=AircraftPosition(
                    transponder_flag="N",
                    call_sign="ELY376",
                    squawk_code="2000",
                    latitude=38.773745,
                    longitude=-9.131191,
                    altitude=374,
                    heading=0,
                ),
                flight_plan=FlightPlan(
                    call_sign="ELY376",
                    flight_plan_type="I",
                    aircraft_type="B738",
                    true_air_speed=405,
                    origin_airport="LPPT",
                    final_cruising_altitude=36000,
                    destination_airport="LLBG",
                    route="IDBID DCT MOMAS UN870 PORLI DCT UREDI",
                ),
                route=Route(
                    point_by_point_route_with_altitude_values="PT544 PT412 MONUR TROIA ODEMI AMSEL",
                    descend_to_waypoint="TROIA",
                    descend_to_altitude=6000,
                ),
            )
        ],
    )


if __name__ == "__main__":
    with open("sweat_box_export.txt", "w") as file:
        file.write(mock().export())
