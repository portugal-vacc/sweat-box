from datetime import datetime, timedelta
from random import randint
from typing import Optional

from fastapi import APIRouter, UploadFile
from starlette.responses import Response, PlainTextResponse
from src.api.helpers import rename_file, unix_to_windows_crlf

from src.models import Scenario
from src.parser import scenario_parser

eobt_api = APIRouter(prefix="/eobt", tags=["EOBT Tools"])


def random_datetime(start, end):
    return start + timedelta(seconds=randint(0, int((end - start).total_seconds())))


def calculate_spawn_time(
    session_start: datetime, eobt: datetime, antecipation_minutes: int = 10
) -> int:
    spawn_time = eobt - timedelta(minutes=antecipation_minutes)
    if spawn_time > session_start:
        relative_time = spawn_time - session_start
        return int(relative_time.seconds / 60)
    return 0


def update_scenario_eobt(
    scenario: Scenario,
    session_utc_start: datetime,
    session_utc_end: datetime,
    delay_start: bool = False,
    delay_time: int = 10,
    exclude_callsigns: list[Optional[str]] = [],
) -> Scenario:
    if exclude_callsigns:
        exclude_callsigns = str(exclude_callsigns[0]).split(",")
    for aircraft in scenario.aircrafts:
        if aircraft.flight_plan.call_sign in exclude_callsigns:
            continue
        eobt = random_datetime(session_utc_start, session_utc_end)
        aircraft.flight_plan.departure_time_est = eobt.strftime("%H%M")
        if not aircraft.route:
            continue
        if delay_start and aircraft.aircraft_position.altitude in range(
            int(scenario.airport_altitude.altitude - 100),
            int(scenario.airport_altitude.altitude + 100),
        ):
            aircraft.route.start = calculate_spawn_time(
                session_utc_start, eobt, delay_time
            )
    return scenario


@eobt_api.post("/update/file")
async def update_file(
    input_file: UploadFile,
    session_utc_start: datetime = datetime.utcnow(),
    session_utc_end: datetime = datetime.utcnow() + timedelta(hours=2),
    delay_start: bool = False,
    delay_time: int = 10,
    exclude_callsigns: list[str] = [],
):
    assert session_utc_start < session_utc_end
    contents = await input_file.read()
    scenario = scenario_parser(contents.decode("utf-8"))
    updated_scenario = update_scenario_eobt(
        scenario,
        session_utc_start,
        session_utc_end,
        delay_start,
        delay_time,
        exclude_callsigns,
    )
    return Response(
        unix_to_windows_crlf(updated_scenario.export()),
        media_type="application/octet-stream",
        headers={
            "Content-Disposition": f"filename={rename_file(input_file.filename)}.txt"
        },
    )


@eobt_api.post("/update/text", deprecated=True, response_class=PlainTextResponse)
async def update_text(
    input_file: UploadFile,
    session_utc_start: datetime = datetime.utcnow(),
    session_utc_end: datetime = datetime.utcnow() + timedelta(hours=2),
    delay_start: bool = False,
    delay_time: int = 10,
):
    assert session_utc_start < session_utc_end
    contents = await input_file.read()
    scenario = scenario_parser(contents.decode("utf-8"))
    updated_scenario = update_scenario_eobt(
        scenario, session_utc_start, session_utc_end, delay_start, delay_time
    )
    return updated_scenario.export()
