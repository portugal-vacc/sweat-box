from fastapi import FastAPI, APIRouter
from fastapi.middleware.cors import CORSMiddleware
from starlette.responses import RedirectResponse

from src.api.scenarios import scenarios_api
from src.api.eobt import eobt_api
from src.api.taxi import taxi_api


app = FastAPI(
    title="Sweat Box API", docs_url="/api/docs", openapi_url="/api/openapi.json"
)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

api = APIRouter(prefix="/api")


@app.get("/", include_in_schema=False)
async def redirect_to_docs():
    return RedirectResponse("/api/docs")


v1_api = APIRouter(prefix="/v1")
v1_api.include_router(eobt_api)
v1_api.include_router(scenarios_api)
v1_api.include_router(taxi_api)
api.include_router(v1_api)

app.include_router(api)
