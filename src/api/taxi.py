from fastapi import APIRouter, Response, UploadFile
from src.api.helpers import rename_file, unix_to_windows_crlf
from src.models import Scenario

from src.parser import scenario_parser


taxi_api = APIRouter(prefix="/taxi", tags=["TAXI Tools"])


def update_scenario_taxi_speed(scenario: Scenario, taxi_speed: int):
    for aircraft in scenario.aircrafts:
        aircraft.sim_data.maximum_taxi_speed = taxi_speed
    return scenario


@taxi_api.post("/update/file")
async def update_file(input_file: UploadFile, taxi_speed: int = 25):
    contents = await input_file.read()
    scenario = scenario_parser(contents.decode("utf-8"))
    updated_scenario = update_scenario_taxi_speed(scenario, taxi_speed)
    return Response(
        unix_to_windows_crlf(updated_scenario.export()),
        media_type="application/octet-stream",
        headers={
            "Content-Disposition": f"filename={rename_file(input_file.filename)}.txt"
        },
    )
