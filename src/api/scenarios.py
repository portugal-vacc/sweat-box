import glob
from pathlib import Path
from typing import Optional

from fastapi import APIRouter
from pydantic import BaseModel
from starlette.responses import FileResponse, Response

from src.parser import scenario_parser

scenarios_api = APIRouter(prefix="/scenarios", tags=["Available Scenarios"])


class Scenario(BaseModel):
    name: str
    category: str
    path: Path


def get_scenarios() -> list[Scenario]:
    return [
        Scenario(name=Path(path).parts[-1], category=Path(path).parts[-2], path=path)
        for path in glob.glob("./scenarios/**/**.txt", recursive=True)
    ]


def get_scenario(
    name: Optional[str] = None, path: Optional[str] = None
) -> Scenario | None:
    try:
        return next(
            file for file in get_scenarios() if file.name == name or file.path == path
        )
    except StopIteration:
        return None


@scenarios_api.get("/", response_model=list[Scenario])
async def get_files():
    return get_scenarios()


@scenarios_api.get("/{name}", response_class=FileResponse)
async def get_file(name: str, path: Optional[str] = None):
    file = get_scenario(name=name, path=path)
    if not file:
        return Response(status_code=404)
    return FileResponse(path=file.path, media_type="text/plain", filename=file.name)


@scenarios_api.get("/{name}/parsed")
async def get_file(name: str, path: Optional[str] = None):
    file = get_scenario(name=name, path=path)
    if not file:
        return Response(status_code=404)
    with open(file.path, "r") as scenario:
        return scenario_parser(scenario.read())
