from datetime import datetime


def unix_to_windows_crlf(raw_text: str) -> str:
    return raw_text.replace("\r", "\r\n")


def rename_file(original_file_name: str) -> str:
    return f"{original_file_name.split('.')[0]}_{datetime.utcnow().strftime('%Y%m%d_%H%M%S')}"
