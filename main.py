import asyncio

from hypercorn import Config
from hypercorn.asyncio import serve

from settings import settings
from src.api.main import app


def run_api() -> None:
    config = Config()
    config.bind = [f"{settings.server_ip}:{settings.server_port}"]
    asyncio.run(serve(app, config))


def main() -> None:
    run_api()


if __name__ == "__main__":
    main()
