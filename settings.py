from dotenv import load_dotenv
from pydantic import BaseSettings

load_dotenv()


class Settings(BaseSettings):
    debug: bool = False
    server_ip: str = "0.0.0.0"
    server_port: str = "9415"

    class Config:
        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
